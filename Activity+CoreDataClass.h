//
//  Activity+CoreDataClass.h
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Activity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Activity+CoreDataProperties.h"
