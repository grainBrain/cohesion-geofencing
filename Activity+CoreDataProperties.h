//
//  Activity+CoreDataProperties.h
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//
//

#import "Activity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Activity (CoreDataProperties)

+ (NSFetchRequest<Activity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *user;
@property (nullable, nonatomic, copy) NSString *mode;
@property (nullable, nonatomic, copy) NSString *time;
@property (nullable, nonatomic, copy) NSString *location;

@end

NS_ASSUME_NONNULL_END
