//
//  Activity+CoreDataProperties.m
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//
//

#import "Activity+CoreDataProperties.h"

@implementation Activity (CoreDataProperties)

+ (NSFetchRequest<Activity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Activity"];
}

@dynamic user;
@dynamic mode;
@dynamic time;
@dynamic location;

@end
