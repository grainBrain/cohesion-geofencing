//
//  AppDelegate.h
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString* uniqueIdentifier;
}

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

@property (nonatomic, strong) NSString* uniqueIdentifier;

@end

