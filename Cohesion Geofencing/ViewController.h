//
//  ViewController.h
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource>
{
    CLLocationManager* locationManager;
    IBOutlet UITextView* textView;
    NSMutableArray* activities;
    IBOutlet UITableView* activityTableView;
}

- (CLRegion*)mapDictionaryToRegion:(NSDictionary*)dictionary;

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) UITextView* textView;
@property (nonatomic, strong) NSArray* activities;
@property (nonatomic, strong) UITableView* activityTableView;

@end

