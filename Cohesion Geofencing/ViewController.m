//
//  ViewController.m
//  Cohesion Geofencing
//
//  Created by Alan Wheat on 4/22/21.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Activity+CoreDataProperties.h"
#import "Activity+CoreDataClass.h"
#import <CoreData/CoreData.h>

@interface ViewController ()

@end

@implementation ViewController

@synthesize locationManager, textView, activities, activityTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.allowsBackgroundLocationUpdates = YES; //so we can track even if app is in the background
    [self initializeRegionMonitoring:[self buildGeofenceData]];
    
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = ad.persistentContainer.viewContext;
    NSError *error;
    /*
    Activity *latest = [NSEntityDescription insertNewObjectForEntityForName:@"Activity" inManagedObjectContext:moc];
    latest.location = @"home";
    latest.mode = @"enter";
    latest.time = @"now";
    latest.user = @"Alan";

    if (![moc save:&error]) {
        NSLog(@"Error saving activity: %@", [error localizedDescription]);
    }
    */
    
    // core data working ?
    NSEntityDescription* e = [NSEntityDescription entityForName:@"Activity" inManagedObjectContext:context];
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity:e];
    NSSortDescriptor* sd = [[NSSortDescriptor alloc] initWithKey:@"user" ascending:YES];
    NSArray* sortDescriptors = [NSArray arrayWithObject:sd];
    [request setSortDescriptors:sortDescriptors];
    activities = [[context executeFetchRequest:request error:&error] mutableCopy];
    NSLog(@"activities:%lu", [activities count]);
    //Activity* activity = [a lastObject];
    for (Activity* activity in activities){
        NSLog(@"%@ %@ %@ at %@", activity.user, activity.mode, activity.location, activity.time);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [activityTableView reloadData];
}

- (NSArray*)buildGeofenceData
{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"regions" ofType:@"plist"];
    NSArray *regionArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    NSMutableArray *geofences = [NSMutableArray array];
    for(NSDictionary *regionDict in regionArray) {
        CLRegion *region = [self mapDictionaryToRegion:regionDict];
        [geofences addObject:region];
    }
    
    return [NSArray arrayWithArray:geofences];
}

- (void) initializeRegionMonitoring:(NSArray*)geofences
{
    if(![CLLocationManager locationServicesEnabled]) {
        NSLog(@"enabled");
        return;
    }
    if (self.locationManager == nil) {
        NSLog(@"exception");
        [NSException raise:@"Location Manager Not Initialized" format:@"You must initialize location manager first."];
    }
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]){
        NSLog(@"unavailable");
        //return; why?
    }
    for(CLRegion *geofence in geofences) {
        [locationManager startMonitoringForRegion:geofence];
    }
}

- (CLRegion*)mapDictionaryToRegion:(NSDictionary*)dictionary
{
    NSString *title = [dictionary valueForKey:@"description"];
    
    CLLocationDegrees latitude = [[dictionary valueForKey:@"latitude"] doubleValue];
    CLLocationDegrees longitude =[[dictionary valueForKey:@"longitude"] doubleValue];
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
    CLLocationDistance regionRadius = [[dictionary valueForKey:@"radius"] doubleValue];
    return  [[CLCircularRegion alloc] initWithCenter:centerCoordinate
                                            radius:regionRadius
                                            identifier:title];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    [self logResult:region withText:@"Entered"];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    [self logResult:region withText:@"Exited"];
}

- (void)logResult:(CLRegion * _Nonnull)region withText:(NSString*)text
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* userid = appDelegate.uniqueIdentifier;

    NSDate * now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *newDateString = [dateFormatter stringFromDate:now];
    
    NSString* result = [NSString stringWithFormat:@"%@ %@ %@ at %@", userid, text, region.identifier, newDateString];
    textView.text = [NSString stringWithFormat:@"%@\n%@", textView.text, result];
    NSLog(@"%@", result);
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    Activity *latest = [NSEntityDescription insertNewObjectForEntityForName:@"Activity" inManagedObjectContext:context];
    latest.location = region.identifier;
    latest.mode = text;
    latest.time = newDateString;
    latest.user = userid;

    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Error saving activity: %@", [error localizedDescription]);
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    NSLog(@"new heading");
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"Now monitoring for %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestAlwaysAuthorization];
            NSLog(@"location services not determined");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            NSLog(@"location services restricted to in use");
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            NSLog(@"location services always available");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"parental controls restricting location services");
            break;
        case kCLAuthorizationStatusDenied:
            NSLog(@"user denied location services");
            break;
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [activities count];
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        Activity* activity = [activities objectAtIndex:indexPath.row];
        UILabel* activityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, activityTableView.frame.size.width * 0.95, 18)];
        activityLabel.backgroundColor = [UIColor clearColor];
        activityLabel.textAlignment = NSTextAlignmentLeft;
        activityLabel.opaque = NO;
        activityLabel.textColor = [UIColor blackColor];
        activityLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        NSString* result = [NSString stringWithFormat:@"%@ %@ %@ %@", activity.user, activity.mode, activity.location, activity.time];
        activityLabel.text = activity.user;
        [cell.contentView addSubview:activityLabel];
        activityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, activityTableView.frame.size.width * 0.95, 25)];
        result = [NSString stringWithFormat:@"%@ %@ %@", activity.mode, activity.location, activity.time];
        activityLabel.text = result;
        [cell.contentView addSubview:activityLabel];

    }
    return cell;
}


@end
