//
//  Cohesion_GeofencingTests.m
//  Cohesion GeofencingTests
//
//  Created by Alan Wheat on 4/22/21.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface Cohesion_GeofencingTests : XCTestCase

@property ViewController *viewController;

@end

@implementation Cohesion_GeofencingTests

- (void)setUp {
    _viewController = [[ViewController alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testMapDictionaryToRegion {
    NSDictionary *regionDict = [NSDictionary dictionaryWithObjectsAndKeys:@"41.2461", @"latitude", @"Home", @"description", @"-95.9972", @"longitude", @"20", @"radius", nil];
    CLRegion* region = [_viewController mapDictionaryToRegion:regionDict];
    XCTAssert([region.identifier isEqualToString:@"Home"]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
