//
//  Activity+CoreDataProperties.h
//  
//
//  Created by Alan Wheat on 4/22/21.
//
//  This file was automatically generated and should not be edited.
//

#import "Activity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Activity (CoreDataProperties)

+ (NSFetchRequest<Activity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *mode;
@property (nullable, nonatomic, copy) NSString *time;
@property (nullable, nonatomic, copy) NSString *user;

@end

NS_ASSUME_NONNULL_END
