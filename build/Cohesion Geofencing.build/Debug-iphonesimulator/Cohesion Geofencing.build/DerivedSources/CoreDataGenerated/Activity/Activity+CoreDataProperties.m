//
//  Activity+CoreDataProperties.m
//  
//
//  Created by Alan Wheat on 4/22/21.
//
//  This file was automatically generated and should not be edited.
//

#import "Activity+CoreDataProperties.h"

@implementation Activity (CoreDataProperties)

+ (NSFetchRequest<Activity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Activity"];
}

@dynamic location;
@dynamic mode;
@dynamic time;
@dynamic user;

@end
